Backbone.emulateHTTP = false; // set to true if server cannot handle HTTP PUT or HTTP DELETE
Backbone.emulateJSON = false; // set to true if server cannot handle application/json requests
var app = {};
(function($){
	$(document).ready(function(){

		$('.cust-list').click(function(){
			$('.cust-list').removeClass('currentActive');
			$(this).toggleClass('currentActive');

			var content = $(this).data('content');
			$('.content-clientes').removeClass('see-content');
			$('#'+content).toggleClass('see-content');
		});
 
 		app.modelo = Backbone.Model.extend({
 			defaults:{
 				idpedido: '',
 				nombre: '', 
				apellido: '', 
				telefono: '', 
				correo: '', 
				fecha: '', 
				detalles: '', 
				direccion: ''
 			}
 		});

		var coleccion = Backbone.Collection.extend({
			url : '/jkguayaberas/wp-admin/admin-ajax.php?action=apipedidos_jk',
			model : app.modelo
		});

		app.copyCollection = new coleccion();
		// Modal para mostrar el detalle del pedido
		app.modalView = Backbone.View.extend({
			tagName : 'div',
			className : 'eldetalle',
			template : _.template( $('#modaldatos').html() ),
			events : {
				'click .save_detalle' : 'save',
				'click .cierradetalle' : 'close',
				// hará el calculo de la fila en donde sucedió el evento 
				// y recalculará el importe global
				'keyup td input' : 'calculo'
			},

			calculo : function(e){
				var descuento = 0;
				// donde ocurrio el keyup obtenemos al padre tr
				var parentTr =  $(e.currentTarget).parents('tr'); 
				// calculamos el subtotal de la fila donde ocurrió el evento
				var subtotal = 
					$(parentTr).children('td:nth-of-type(4)').html()
					*
					$(parentTr).children('td:nth-of-type(5)').children().val();
				// agregamos el resultado en la fila de subtotal
				$(parentTr).children('td:nth-of-type(6)').children('span').html( subtotal );
				// hacemos el calculo para el descuento si hay descuento
				if( $(parentTr).children('td:nth-of-type(7)').children().val() != '' ){
					descuento = ( 
						( 	$(parentTr).children('td:nth-of-type(7)').children().val()  ) 
							* 
							subtotal 
						) / 100;	
				}
				// agreamos el total en la fila donde ocurrio el evento
				$(parentTr).children('td:nth-of-type(8)').children('span').html(subtotal - descuento);
				
				//ahora procedemos a calcular las cantidades globales
				var trs = $('.table-modal tr');

				var descuentoGlobal = 0;
				for (var i = 0; i < $(trs).length; i++) {
					if( $(parentTr).children('td:nth-of-type(7)').children().val() != '' ){

						descuentoGlobal += Number(
						Number( $(trs[i]).children('td:nth-of-type(6)').children('span').html() )
						-
						Number( $(trs[i]).children('td:nth-of-type(8)').children('span').html()  )
						);

					}
				};
				$('#descuentoGlobal').html(descuentoGlobal);
				$('#subtotalGlobal').html( descuentoGlobal );

			},

			initialize : function(){
				this.listenTo(this.model, 'change', this.render);
			},
			render : function(){	
				
				this.$el.html( this.template( this.model.toJSON() ) );
				this.$el.attr( 'id', 'modal'+this.model.cid );
				return this;
			},
			close : function(e){
			
				$('.datosdelcliente').toggleClass('showdatos');
			},
			save : function(){
				var trs = this.$('.table-modal tr');
				var detalles = [];
				
				for(var y = 0; y < $(trs).length; y++ ){
					
					detalles.push({ 
						img : $(trs[y]).find('td:nth-of-type(1) img').attr('src'),
						producto : $(trs[y]).find('td:nth-of-type(2)').html(),
						talla : $(trs[y]).find('td:nth-of-type(3)').html(),
						precio : $(trs[y]).find('td:nth-of-type(4)').html(),
						cantidad : $(trs[y]).find('td:nth-of-type(5) input').val(),
						descuento : $(trs[y]).find('td:nth-of-type(7) input').val()
					});

				}
				// 06860EAA2AEB8601
				this.model.save(
				{
					detalles : JSON.stringify(detalles)		
				},
				{
					type : 'POST',
					wait:true,					
					success: function (exito){
						alert(exito);
					}, 
					error: function (error){}
				}
				);
			},
		});
		app.singleView = Backbone.View.extend({

			tagName : 'tr',
			template : _.template( $('#tpl-pedidos').html() ),
			
			events : {
				'click .ver' : 'infocliente',
				'click .removecliente' : 'eliminar',
			},

			initialize : function(){
				this.listenTo(this.model, 'change', this.render);
				this.listenTo(this.model, 'destroy', this.remove); 
			},

			render : function(){
				this.$el.html( this.template( this.model.toJSON() ) );
				this.$el.addClass( 'trc'+this.model.get('id')+' ' );
				this.$el.attr('data-tipo',this.model.get('id'));
				return this;
			},
			infocliente : function(){				
				if( $('.eldetalle').attr('id') != 'modal'+this.model.get('id') ){
					var modal = new app.modalView({ model : this.model }).render().el;
					$('.datosdelcliente').html( modal );	
					$('.datosdelcliente').toggleClass('showdatos');
				}else{
					$('.datosdelcliente').toggleClass('showdatos');
				}
			},			
			
			eliminar : function(){
				var self = this;
				$('.datosdelcliente').toggleClass('showdatos');
				$('.alertaremove').toggleClass('showdatos');
			
				$('.aceptar').click(function(){
					self.model.destroy();					
				});
			}
		});

		app.masterView = Backbone.View.extend({
			el : '.content-pedidos',
			events : {
				'click .closedatos' : 'ocultar',
				'click .cancelar' : 'togle',
				'keyup .busqueda' :'doSearch'
			},
			
			initialize : function(){
				this.listenTo( app.copyCollection, 'add', this.list ); 
				// this.listenTo( app.copyCollection, 'change', this.list ); 
				app.copyCollection.fetch();
				this.cliente;
				this.distribuidor;
			},

			render : function(){},

			list : function(elCliente)
			{
				this.cliente = new app.singleView({ model: elCliente });				
				this.$('#the-list').append( this.cliente.render().el );
			},

			ocultar : function(event){
				
				$('.datosdelcliente').toggleClass('showdatos');
			},

			togle : function(){
				$('.datosdelcliente').toggleClass('showdatos');
				$('.alertaremove').toggleClass('showdatos');
			},

			doSearch : function(e)
			{
			var tableReg = document.getElementById('the-list');
			var searchText = $(e.currentTarget).val().toLowerCase(); //document.getElementById('searchTerm').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";
 
			// Recorremos todas las filas con contenido de la tabla
			for (var i = 1; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = 'block';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
		}

		});
		app.vistaenHtml = new app.masterView();

	}); /* Document Query */
	
})( jQuery );