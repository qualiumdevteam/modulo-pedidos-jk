<?php 
/*
Plugin Name: Panel Pedidos
Plugin URI: http://jkGuayaberas.com
Description: Panel para mostrar pedidos
Version: 1.0
Author: Alberto Qualium
Author URI: JKGuayaberas
License: GPLv2
*/
define('CONTOVER_PATH', plugin_dir_path(__FILE__));
define('CONTOVER_SLUG2', 'pedidos');
define('CONTOVER_URI2', get_bloginfo('url') . '/wp-content/plugins/pedidos');

add_action('admin_print_scripts', 'pedidosscripts');

function pedidosscripts(){
	$root = 'http://localhost:8888';
	$url = sprintf("%s%s%s","http://",$_SERVER['HTTP_HOST'],$_SERVER['REQUEST_URI']);

	if($url == $root.'/jkguayaberas/wp-admin/admin.php?page=pedidos' ) 
	{
	    wp_enqueue_script( 'pedidosjs', CONTOVER_URI2 . '/pedidos.js', array( 'jquery','underscore','backbone' ) );
	    wp_enqueue_style( 'iconpedidoscss', CONTOVER_URI2 .'/icons/flaticon.css' );
	    wp_enqueue_style( 'pedidoscss', CONTOVER_URI2 .'/pedidos.css' );
	}
}

add_action('admin_menu', 'pedidos_plugin_menu');
function pedidos_plugin_menu() 
{
    add_menu_page( 'Pedidos', 'Pedidos', 'edit_posts', 'pedidos', 'pedidos_plugin_options' ); 
}

function pedidos_plugin_options() {
	if (!current_user_can('edit_posts'))  {
		wp_die( __('No tiene suficientes permisos para acceder a esta página.') );
	} 
?>
	<div class="wrap">
        <div class="registrarpedido button-primary" style="float:right;">Registrar Pedido</div>
        <h1>Pedidos</h1>
        <div class="row">
        	
        	<div class="content-pedidos">
        		<!-- <input class="busqueda" type="text" value=""> -->
	        	<div class="pedidos-list">
	        		<?php include 'tabla-pedidos.php'; ?>
	        	</div>

	        	<div class="datosdelcliente">
	        		<!-- Filas de pedidos -->
	        	</div>
	        	
	        	<div class="registrapedido">
	        		<!-- Filas de pedidos -->
	        		<div class="registro">
	        			<span>x</span>
	        			<h1> Registrar Pedido</h1>
	        			<input type="text" placeholder="Nombre del Cliente">
	        			<input type="text" placeholder="correo">
	        			<input type="text" placeholder="Teléfono">
	        			<input type="text" placeholder="Fecha">
	        			<input type="text" placeholder="Dirección">
	        			<input type="text" placeholder="Producto">
	        			<input type="text" placeholder="Precio">
	        			<input type="text" placeholder="Cantidad">
	        			<input type="text" placeholder="Descuento">
						<input type="text" placeholder="Total">
	        			<div class="button-primary" style="float:right;">Guardar</div>
	        		</div>
	        	</div>

        	</div>

        </div>
	</div>

	<script id="modaldatos" type="text/template">
		 
		<div class="cierradetalle">x</div>
		<% datos = JSON.parse(detalles);  %>
		<h3>Cliente <%- nombre %> <%- apellido %></h3>
		<div class="content-table">
			<table cellspacing="0" border="0" class="wwp-list-table widefat striped posts">
				<thead>
					<tr class="cabecera">
						<th> Imagen </th>
						<th> Producto </th>
						<th> Talla </th>
						<th> Precio </th>
						<th> Cantidad</th>
						<th> Subtotal </th>
						<th> % de Desc </th>
						<th> Total </th>
					</tr>
				</thead>
				<tbody class="table-modal">
					<%  var subtotal = 0; var globalSubtotal=0; var descuentos = 0; %>
					<% for( var x =0;x < datos.length; x++){ %>
					<% subtotal = ( datos[x].cantidad * datos[x].precio ) %>
					<% 
						if( datos[x].descuento  ){
							descuentos = ( ( subtotal * datos[x].descuento ) / 100 ) ;	
						}else{
							descuentos = subtotal;
						}
						
					%>
					<% globalSubtotal += Number(subtotal); %>
					<tr>
						<td><img class="fotoproducto" src="<%- datos[x].img %>"></td>
						<td><%- datos[x].nombreprod %></td>
						<td><%- datos[x].talla %></td>
						<td><%- datos[x].precio %></td>
						<td><input type="text" value="<%- datos[x].cantidad %>"></td>
						<td>$<span><%- subtotal %></span></td>
						<td><input type="text" value="<%- datos[x].descuento %>"></td>
						<td>$<span><%- descuentos %></span></td>
					</tr>
					<% } %>
				</tbody>			
			</table>
		</div>
		<div class="totales">
			<p>
				<label><b>Subtotal:</b> </label>
				<span id="subtotalGlobal" ><%- globalSubtotal %></span>
			</p>
			<p>
				<label><b>Descuento en moneda:</b> </label>
				<span id="descuentoGlobal"><%- globalSubtotal %></span>
			</p>
			<p>
				<label><b>Iva:</b> </label><%- globalSubtotal %>
			</p>
			<p>
				<label><b>Total:</b> </label>
				<span><%- globalSubtotal %></span>
			</p>
			<div style="clear:both"></div><br>
			<div class="save_detalle button-primary">Guardar</div>
		</div>		
	</script>
	<script id="tpl-pedidos" type="text/template">
	
		<th colspan="2" scope="col" class="manage-column column-cb check-column">
			<%- nombre %> <%- apellido %>
		</th>
		<td colspan="2"> <%- correo %> </td>
		<td colspan="2"> <%- telefono %>  </td> 
		<td colspan="2"> <%- fecha %> </td> 
		<td colspan="3"> <%- direccion %> </td> 
		<td colspan="1" class="icon-detalle"> 
			<span class="flaticon-eyes12 ver"></span>
			<span class="flaticon-transport754 car"></span>
			<input type="text" value="<%- detalles %>" style="display:none;">
		</td>
	</script>
	<script>
		jQuery('.registrarpedido').click(function(){

			jQuery('.registrapedido').toggleClass('showdatos');

		});
		jQuery('span').click(function(){
			jQuery(this).parents('.registrapedido').toggleClass('showdatos');
		})
	</script>
<?php } ?>
